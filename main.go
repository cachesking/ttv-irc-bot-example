package main

import (
	"github.com/gempir/go-twitch-irc/v2"
	"github.com/joho/godotenv"
	"log"
	"os"
)

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	HOST := os.Getenv("HOST")
	TOKEN := os.Getenv("TOKEN")
	CHANNEL := "cachesking"

	log.Printf("Connecting to %s...\n", CHANNEL)

	client := twitch.NewClient(HOST, TOKEN)

	client.Join(CHANNEL)

	client.OnConnect(func() {
		log.Printf("Connected.\n")
	})

	client.OnPrivateMessage(func(msg twitch.PrivateMessage) {
		log.Printf("%s: %s\n", msg.User.Name, msg.Message)
	})

	err := client.Connect()
	if err != nil {
		log.Fatal(err)
	}
}
