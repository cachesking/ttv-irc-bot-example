# Twitch TV IRC bot example

An example of creating an irc bot for twitch. This project is written in go and has two dependencies.

 - [godotenv](ghithub.com/joho/godotenv) to load environment variables

 - [go-twitch-irc](github.com/go-twitch-irc) to handle irc

## Set Up

_You may export the following environment variables if two dependencies is one too many_

Rename the `example.env` file to `.env` and update accordingly.

HOST: twitch user to connect

TOKEN: OAuth token

## Get a token

1. sign in to [twitch](https://twitch.tv)
2. visit `https://twitchapps.com/tmi/`
3. authorize twitch to vend your user's token
4. update the `TOKEN` entry in `.env`

## To Run

Get the dependencies by running `go get` and then `go run main.go`.

The make file includes:
 - `make dependencies`: installs dependencies

 - `make deps`: types less keys to install dependencies

 - `make run`: runs the bot

# TODO:

  - set up `make build` targets to other Architectures.
