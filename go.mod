module gitlab.com/cachesking/ttv-bot-example

go 1.14

require (
	github.com/gempir/go-twitch-irc/v2 v2.4.0
	github.com/joho/godotenv v1.3.0
)
